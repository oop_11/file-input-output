/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.fileinputoutput;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Admin
 */
public class Computer implements Serializable {
    private int hand ;//hand: 0:rock, 1:scissors , 2:paper
    private int playerhand;//hand: 0:rock, 1:scissors , 2:paper
    private int win,lose,draw;
    private int status;
    
    public Computer(){
        
    }
    private int choob(){
        return ThreadLocalRandom.current().nextInt(0,3);
    }
    
    public int paoYingChoob(int playerHand){ //win: 1,draw: 0,lose: -1
        this.playerhand = playerHand;
        this.hand = choob();
        this.hand = ThreadLocalRandom.current().nextInt(0,3);
        
        if(this.playerhand == this.hand){
            draw++;
            status=0;
            return 0;
        }
        if(this.playerhand == 0 && this.hand == 1){
            win++;
            status=1;
            return 1;
        }
        if(this.playerhand == 1 && this.hand == 2){
            win++;
            status=1;
            return 1;
        }
        if(this.playerhand == 2 && this.hand == 0){
            win++;
            status=1;
            return 1;
        }
        lose++;
        status=-1;
        return -1;
    }

    public int getHand() {
        return hand;
    }

    public int getPlayerhand() {
        return playerhand;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public int getStatus() {
        return status;
    }
    
}
