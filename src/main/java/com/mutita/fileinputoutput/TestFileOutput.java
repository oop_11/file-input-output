/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.fileinputoutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class TestFileOutput {
    public static void main(String[] args) {
        FileOutputStream fos =null;
        ObjectOutputStream oos =null;
        try {
            Dog dog =new Dog("Dang",5);// dog.obj
            Rectangle rec = new Rectangle(5,4);
            File file = new File("dog.obj");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(dog);
            oos.writeObject(rec);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            System.out.println("File Not Found!!!");
        } catch (IOException ex) {
            Logger.getLogger(TestFileOutput.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestFileOutput.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
