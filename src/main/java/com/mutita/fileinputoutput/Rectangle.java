/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.fileinputoutput;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class Rectangle implements Serializable{
    private int width;
    private int heigth;

    public Rectangle(int width, int heigth) {
        this.width = width;
        this.heigth = heigth;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeigth() {
        return heigth;
    }

    public void setHeigth(int heigth) {
        this.heigth = heigth;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "width=" + width + ", heigth=" + heigth + '}';
    }
    
    
}
